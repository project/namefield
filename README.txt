namefield

README

Description
------------

This module defines a CCK field for Names.  The field has the following
sub-fields:  
  Prefix, First, Middle, Last, Suffix

For example:
  Mr. Michael J Fox Jr.

In configuring the field, you can set any of these off, or required.

Also, it gives you a "preferred name".  In configuring the field, you can set
many aspects of how the displayed name is calculated from the "Legal" and
"Preferred" components.  All filters and sorts are done on the calculated
display name.


Limitations
-----------

This module does not yet support multiple values.  This module is considered
experimental until this feature is implemented.

Other features on the wishlist are listed in TODO.txt.


Installation
------------

Unpack the tarball and install in your modules directory.  Enable it on the
modules admin page.  In order to install modules, though, you'll need to make
the directory you intend to install to writable by the web-server user.
